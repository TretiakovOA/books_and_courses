﻿using Models;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Newtonsoft.Json;
using static System.Console;
using System.Linq;
using System;

namespace BooksAndCourses.Console
{
    class Program
    {
        private delegate void SerializationHandler(Library library, string fileName);

        static void Main(string[] args)
        {
            Library library = LibInitializer.InitLibrary();

            //SerializationBinary(library, "library");
            //SerializationXml(library, "library");
            //SerializationJson(library, "library");

            //InvokeDelegate(library);
            UseLinq(library);

            ReadKey();
        }

        private static void UseLinq(Library library)
        {
            var authors1 = from resourse in library.Resources
                           where resourse.Authors.Length == 1
                           orderby resourse.Title
                           select resourse.Authors.First();
            //foreach(var a in authors1.Distinct())
            //{
            //    WriteLine(a);                                
            //}
            //var authors2 = library.Resources
            //    //.Where(r => r.Authors.Length == 2)
            //    //.OrderBy(r => r.Title)
            //    .Select(r => r);
                //.Select(r => r.Authors).First();
            //foreach (var a in authors2)
            //{
            //    WriteLine(a);
            //}
            WriteLine(library.Resources.Min(a => a.Year));

            var pages = library.Resources.OfType<Book>()
                .Select(r => r.Pages).Sum();
            WriteLine(pages);
        }

        private static void InvokeDelegate(Library library)
        {
            SerializationHandler serialization = null;

            WriteLine("Выберите тип сериализации: 1. Бинарная. 2. XML. 3. JSON. 4. Все");
            string input = ReadLine();
            if (int.TryParse(input, out int iInput))
            {
                switch (iInput)
                {
                    case 1:
                        serialization = SerializationBinary;
                        break;
                    case 2:
                        serialization = SerializationXml;
                        break;
                    case 3:
                        serialization = SerializationJson;
                        break;
                    case 4:
                        serialization = SerializationBinary;
                        serialization += SerializationXml;
                        serialization += SerializationJson;
                        break;
                    default:
                        WriteLine("Неверное число");
                        break;
                }
            }
            else
            {
                WriteLine("Не число");
            }
            
            serialization?.Invoke(library, "library");
        }

        private static void SerializationBinary(Library library, string fileName)
        {
            fileName += ".bin";
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fs, library);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                Library newLibrary = (Library) binaryFormatter.Deserialize(fs);                
            }
        }

        private static void SerializationXml(Library library, string fileName)
        {
            fileName += ".xml";
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Library));
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                xmlSerializer.Serialize(fs, library);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                Library newLibrary = (Library)xmlSerializer.Deserialize(fs);
            }
        }

        private static void SerializationJson(Library library, string fileName)
        {
            fileName += ".json";
            JsonSerializer jsonSerializer = new JsonSerializer();
            jsonSerializer.Converters.Add(new LibResourceConverter());
            using (StreamWriter sw = new StreamWriter(fileName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                jsonSerializer.Serialize(writer, library);
            }
            using (StreamReader sr = File.OpenText(fileName))
            {
                Library newLibrary = (Library)jsonSerializer.Deserialize(sr, typeof(Library));
            }
        }

    }
}
