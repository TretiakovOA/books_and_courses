﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using BooksAndCourses.Web.Models;

namespace BooksAndCourses.Web
{
    public class Program
    {
    public static void Main(string[] args)
    {
      var host = BuildWebHost(args);

      using (var scope = host.Services.CreateScope()) { 
        try
        {
          var services = scope.ServiceProvider;
          var context = services.GetRequiredService<BooksContext>();
          DbInitializer.Initialize(context);
        }
        catch (Exception)
        {
          //to log
        }
      }
      host.Run();
    }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
