﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksAndCourses.Web.Models
{
    public class Book
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookId { get; set; }
        
        [Required]
        [Display(Name = "Наименование")]
        [StringLength(255)]
        public string Title { get; set; } 

        [Display(Name = "Год")]
        public int? Year { get; set; }
        
        [Display(Name = "Обложка")]
        [StringLength(255)]
        public string PathToCover { get; set; }

        [Display(Name = "Описание")]
        [StringLength(255)]
        public string PathToDescription { get; set; }
        
        [Display(Name = "Авторы")]
        public ICollection<Author> Authors { get; set; }
        
        [Display(Name = "Главы")]
        public ICollection<Chapter> Chapters { get; set; }

        [Display(Name = "Количество страниц")]
        public int? Pages { get; set; }
        
        [Display(Name = "Страница")]
        public int? FirstPage { get; set; }
        
        [Display(Name = "Издательство")]
        [StringLength(255)]
        public string Publishment { get; set; }

        [Display(Name = "Издание")]
        public int? Edition { get; set; }

        [NotMapped]
        [Display(Name = "Авторы")]
        public List<Author> AuthorsViewModel { get; set; }

        [NotMapped]
        [Display(Name = "Новый автор")]
        public string NewAuthor { get; set; }
    }
}
