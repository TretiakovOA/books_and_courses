﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksAndCourses.Web.Models
{
    public class Chapter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChapterId { get; set; }
        
        [Required]
        [Display(Name = "Наименование")]
        [StringLength(255)]
        public string Title { get; set; } 

        [Display(Name = "Порядковый номер")]
        public int Order {get; set;}
        
        [Display(Name = "Количество страниц")]
        public int? Pages { get; set; }
        
        [Display(Name = "Страница")]
        public int? FirstPage { get; set; }

        public ICollection<Chapter> SubChapters { get; set; }

        public int BookId { get; set; }
        public int? ParentChapterId { get; set; }
    }
}
