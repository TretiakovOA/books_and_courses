﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksAndCourses.Web.Models
{
    public class InternetResource
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InternetResourceId { get; set; }
        
        [Required]
        [Display(Name = "Наименование")]
        [StringLength(255)]
        public string Title { get; set; } 

        [Display(Name = "Год")]
        public int? Year { get; set; }
        
        [Display(Name = "Обложка")]
        [StringLength(255)]
        public string PathToCover { get; set; }

        [Display(Name = "Описание")]
        [StringLength(255)]
        public string PathToDescription { get; set; }
                
        public ICollection<Author> Authors { get; set; }

        [Required]
        [Display(Name = "Основная ссылка")]        
        public string MainLink { get; set; } 

        [NotMapped]
        [Display(Name = "Авторы")]
        public List<Author> AuthorsViewModel { get; set; }

        [NotMapped]
        [Display(Name = "Новый автор")]
        public string NewAuthor { get; set; }
    }
}
