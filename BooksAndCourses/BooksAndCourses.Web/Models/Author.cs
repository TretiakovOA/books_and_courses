﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksAndCourses.Web.Models
{
    public class Author
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }
        
        [Required]
        [Display(Name = "Наименование")]
        [StringLength(255)]
        [Column("Author")]
        public string Title { get; set; } 

        public int? BookId { get; set; }        
        public int? InternetResourceId { get; set; }
    }
}
