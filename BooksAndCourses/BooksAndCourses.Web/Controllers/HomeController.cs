﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BooksAndCourses.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace BooksAndCourses.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly BooksContext _context;

        public HomeController(BooksContext ctx)
        {
            _context = ctx;
        }

        // GET: Home
        public IActionResult Index()
        {                                            
            return View(GetResources());
        }

        // GET: Home/CreateBook
        public IActionResult CreateBook()
        {
            return View();
        }

        // POST: Home/CreateBook
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBook([Bind("BookId,Title,Year,PathToCover,PathToDescription,Pages,FirstPage,Publishment,Edition,AuthorsViewModel,NewAuthor")] Book book,
            string AddAuthor, string RemoveAuthor)
        {
            if (!string.IsNullOrEmpty(AddAuthor))
            {
                ModelState.Clear();
                await AddNewAuthor(book);
                return View(nameof(CreateBook), book);
            }
            if (!string.IsNullOrEmpty(RemoveAuthor))
            {
                ModelState.Clear();
                string toDelete = RemoveAuthor.Remove(0, "Удалить автора ".Length);
                var authorToDelete = book.AuthorsViewModel.FirstOrDefault(a => a.Title.Equals(toDelete));
                book.AuthorsViewModel.Remove(authorToDelete);
                return View(nameof(CreateBook), book);
            }
            if (ModelState.IsValid)
            {
                await AddNewAuthor(book);
                _context.Add(book);
                await _context.SaveChangesAsync();

                int id = book.BookId;
                foreach (var author in book.AuthorsViewModel)
                {
                    author.BookId = id;
                    _context.Add(author);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(book);            
        }                

        // GET: Home/DetailsBook/5
        public async Task<IActionResult> DetailsBook(int? id)
        {
            if (id == null) return NotFound();            
            var book = await _context.Books.SingleOrDefaultAsync(m => m.BookId == id);
            if (book == null) return NotFound();

            AddAuthorsAndChapters(book);
            return View(book);
        }

        // GET: Home/EditBook/5
        public async Task<IActionResult> EditBook(int? id)
        {
            if (id == null) return NotFound(); 
            var book = await _context.Books.SingleOrDefaultAsync(m => m.BookId == id);
            if (book == null) return NotFound(); 

            AddAuthorsAndChapters(book); 
            book.AuthorsViewModel = book.Authors.ToList();
            return View(book);
        }

        // POST: Home/EditBook/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(int id, [Bind("BookId,Title,Year,PathToCover,PathToDescription,Pages,FirstPage,Publishment,Edition,Authors,NewAuthor")] Book book,
            string AddAuthor, string RemoveAuthor)
        {
            if (id != book.BookId) return NotFound(); 
            if (!string.IsNullOrEmpty(AddAuthor))
            {                
                ModelState.Clear();
                if (book.Authors == null) book.Authors = GetAuthorsOfBook(book);     
                book.AuthorsViewModel = book.Authors.ToList();
                await AddNewAuthor(book);                
                return View(nameof(EditBook), book);
            }
             if (!string.IsNullOrEmpty(RemoveAuthor))
            {                
                ModelState.Clear();
                if (book.Authors == null) book.Authors = GetAuthorsOfBook(book);
                string toDelete = RemoveAuthor.Remove(0, "Удалить автора ".Length);
                var authorToDelete = book.Authors.FirstOrDefault(a => a.Title.Equals(toDelete));
                if (authorToDelete.BookId > 0) {
                    _context.Remove(authorToDelete);
                    await _context.SaveChangesAsync();
                }
                book.AuthorsViewModel = GetAuthorsOfBook(book);
                return View(nameof(EditBook), book);
            }
            if (ModelState.IsValid)
            {
                try
                {                    
                    await AddNewAuthor(book);            
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.BookId)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(book);
        }

        // GET: Home/DeleteBook/5
        public async Task<IActionResult> DeleteBook(int? id)
        {
            if (id == null) return NotFound();
            var book = await _context.Books.SingleOrDefaultAsync(m => m.BookId == id);
            if (book == null) return NotFound();

            AddAuthorsAndChapters(book); 
            return View(book);
        }

        // POST: Home/DeleteBook/5
        [HttpPost, ActionName("DeleteBook")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteBookConfirmed(int id)
        {
            var book = await _context.Books.SingleOrDefaultAsync(m => m.BookId == id);
            AddAuthorsAndChapters(book);
            foreach(var author in book.Authors)
            {
                _context.Authors.Remove(author);
            }
            if (book.Chapters != null) 
            {
                foreach(var chapter in book.Chapters)
                {
                    if (chapter.SubChapters != null) 
                    {
                        foreach(var subchapter in chapter.SubChapters)
                        {
                            _context.Chapters.Remove(subchapter);
                        }
                    }
                    _context.Chapters.Remove(chapter);
                }
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Полезные ресурсы для изучения С#.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Смотри репозиторий на Bitbucket.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<Author> GetAuthorsOfBook(Book book)
        {
            return _context.Authors.Where(a => a.BookId == book.BookId).ToList();
        }
        
        private async Task AddNewAuthor(Book book)
        {
            if (!string.IsNullOrEmpty(book.NewAuthor))
            {
                if (book.AuthorsViewModel == null) book.AuthorsViewModel = new List<Author>();
                var author = new Author {Title = book.NewAuthor, BookId = book.BookId};
                book.AuthorsViewModel.Add(author);
                if (author.BookId > 0) {
                    _context.Add(author);
                    await _context.SaveChangesAsync();
                }
                book.NewAuthor = string.Empty;
            }            
        }
        
        private void AddAuthorsAndChapters(Book book)
        {
            book.Authors = GetAuthorsOfBook(book);
            var chapters = _context.Chapters.Where(c => c.BookId == book.BookId && c.ParentChapterId == null)
                .OrderBy(c => c.Order).ToList();
            foreach (var chapter in chapters)
            {
                chapter.SubChapters = _context.Chapters.Where(c => c.BookId == book.BookId && c.ParentChapterId == chapter.ChapterId)
                    .OrderBy(c => c.Order).ToList();
            }
            book.Chapters = chapters;
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.BookId == id);
        }

        private List<LibResourceViewModel> GetResources()
        {
            List<LibResourceViewModel> resources = new List<LibResourceViewModel>();
            var books = _context.Books;
            var sites = _context.InternetResources;            
            foreach (var b in books)
            {
                var model = CreateModel(book: b);
                resources.Add(model);
            }
            foreach (var s in sites)
            {
                var model = CreateModel(site: s);
                resources.Add(model);
            }                        
            return resources;     
            
            LibResourceViewModel CreateModel(Book book = null, InternetResource site = null)
            {
                if (book == null && site == null) return null;
                LibResourceViewModel model = new LibResourceViewModel();
                model.Type = book != null ? LibResourceType.Book : LibResourceType.InternetResource;
                model.Id = book?.BookId ?? site.InternetResourceId;
                
                var authors = book != null ? GetAuthorsOfBook(book) :
                    _context.Authors.Where(a => a.InternetResourceId == site.InternetResourceId).ToList();
                if (authors.Count >= 1) { 
                    var author = authors.FirstOrDefault().Title;
                    if (authors.Count > 1) author += " и др.";
                    model.Author = author;
                }
                model.Title = book?.Title ?? site.Title;
                return model;
            }
        }
    }
}
