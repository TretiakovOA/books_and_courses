﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Snippets
{
    class Program
    {
        private delegate double OurOperationHandler(double input);
        
        static void Main(string[] args)
        {
            //InvokeDelegate();
            //InvokeEvent();
            //UseAnonymous();
            //UseLambda();
            UseSwap();
        }

        private static void UseSwap()
        {
            int i1 = 10, i2 = 20;
            double d1 = 3.3, d2 = 4.4;
            string s1 = "Hello", s2 = "World!";
            Swap(ref i1, ref i2);
            Swap(ref d1, ref d2);
            Swap(ref s1, ref s2);
        }

        private static void UseLambda()
        {
            OurOperationHandler handler = null;
            do
            {
                double number = new Random().Next(1000000) / 100000.0;
                if (number < 5) handler = x => Math.Sqrt(x);
                else handler = x => x * x;
                double result = handler.Invoke(number);
                WriteLine(result);
            } while (ReadLine() != "q");
        }

        private static void UseAnonymous()
        {
            OurOperationHandler handler = null;
            do
            {
                double number; //= new Random().Next(1000000) / 100000.0;
                number = 28;
                if (number < 5) handler = delegate (double x)
                {
                    return Math.Sqrt(x);
                };
                else handler = delegate (double x)
                {
                    return x * x;
                };
                number = -30;
                double result = handler.Invoke(number);
                WriteLine(result);
            } while (ReadLine() != "q");
        }

        //private static void InvokeDelegate()
        //{
        //    OurOperationHandler handler = null;
        //    do
        //    {
        //        double number = new Random().Next(1000000) / 100000.0;
        //        if (number < 5) handler = Sqrt;
        //        else handler = Sqr;

        //        double result = handler.Invoke(number);
        //        WriteLine(result);
        //    } while (ReadLine() != "q");
        //}

        //private static void InvokeEvent()
        //{
        //    _ourEvent = null;
        //    do
        //    {
        //        double number = new Random().Next(1000000) / 100000.0;
        //        if (number < 5) _ourEvent = Sqrt;
        //        else _ourEvent = Sqr;

        //        double result = _ourEvent.Invoke(number);
        //        WriteLine(result);
        //    } while (ReadLine() != "q");
        //}

        //static double Sqrt(double x)
        //{
        //    return Math.Sqrt(x);
        //}

        //static double Sqr(double x)
        //{
        //    return x * x;
        //}

        static void Swap<T> (ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }
    }
}
