﻿using Models;
using Models.AbstractClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksAndCourses.Database
{
    public static class AdoNetHelper
    {
        private const string _connectionString =
            @"Data Source=STUDENT-06-PC\SQLEXPRESS;Initial Catalog=BooksAndCourses;Integrated Security=True;Connect Timeout=30;";

        #region Create
        
        public static void InitLibraryFirstTime(Library library)
        {
            foreach (var resource in library.Resources)
            {
                var id = AddResource(resource);
                resource.Id = id;
            }
        }

        public static int AddResource(LibResource resource)
        {
            if (!ValidateResource(resource)) return 0;
            int resourceId;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand ();
                cmd.Connection = connection;

                SqlParameter title = new SqlParameter("@Title", SqlDbType.NVarChar);
                SqlParameter year = new SqlParameter("@Year", SqlDbType.Int);
                SqlParameter pathToCover = new SqlParameter("@PathToCover", SqlDbType.NVarChar);
                SqlParameter pathToDescription = new SqlParameter("@PathToDescription", SqlDbType.NVarChar);

                cmd.Parameters.Add(title).Value = resource.Title;
                cmd.Parameters.Add(year).Value = resource.Year;
                cmd.Parameters.Add(pathToCover).Value = (object) resource.PathToCover ?? DBNull.Value; 
                cmd.Parameters.Add(pathToDescription).Value = (object)resource.PathToDescription ?? DBNull.Value;

                string tableName, varParamNames, varParams;
                switch (resource)
                {
                    case Book book:
                        cmd.Parameters.Add("@Pages", SqlDbType.Int).Value = book.Pages;
                        cmd.Parameters.Add("@FirstPage", SqlDbType.Int).Value = book.FirstPage;
                        cmd.Parameters.Add("@Publishment", SqlDbType.NVarChar).Value = book.Publishment;
                        cmd.Parameters.Add("@Edition", SqlDbType.Int).Value = book.Edition;
                        tableName = "dbo.Book";
                        varParamNames = "Pages, FirstPage, Publishment, Edition";
                        varParams = "@Pages, @FirstPage, @Publishment, @Edition";
                        break;
                    case VideoCourse video:
                        cmd.Parameters.Add("@Length", SqlDbType.Int).Value = video.Length;                        
                        tableName = "dbo.VideoCourse";
                        varParamNames = "Length";
                        varParams = "@Length";
                        break;
                    case InternetResource site:
                        cmd.Parameters.Add("@MainLink", SqlDbType.NVarChar).Value = site.MainLink;
                        tableName = "dbo.InternetResource";
                        varParamNames = "MainLink";
                        varParams = "@MainLink";
                        break;
                    default: return 0;
                }
                cmd.CommandText = $"INSERT INTO {tableName} (Title, Year, PathToCover, PathToDescription, " +
                    $"{varParamNames}) VALUES(@Title, @Year, @PathToCover, @PathToDescription, {varParams}) " +
                    $"SELECT SCOPE_IDENTITY()";
                connection.Open();
                resourceId = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            AddAdditionalEntities(resource, resourceId);
            return resourceId;
        }

        private static void AddAdditionalEntities(LibResource resource, int resourceId)
        {
            string libResourceType;
            switch (resource)
            {
                case Book book:
                    libResourceType = "Book";
                    if (book.Chapters != null)
                    {
                        foreach (var chapter in book.Chapters)
                        {
                            chapter.Id = AddChapter(chapter, resourceId, 0);
                        }
                    }
                    break;
                case VideoCourse video:
                    libResourceType = "VideoCourse";
                    if (video.Sections != null)
                    {
                        foreach (var section in video.Sections)
                        {
                            section.Id = AddVideoSection(section, resourceId, 0);
                        }
                    }
                    break;
                case InternetResource site:
                    libResourceType = "InternetResource";
                    break;
                default: return;
            }

            if (resource.Themes != null)
            {
                foreach (var theme in resource.Themes)
                {
                    AddTheme(theme, libResourceType, resourceId);
                }
            }
            if (resource.Authors != null)
            {
                foreach (var author in resource.Authors)
                {
                    AddAuthor(author, libResourceType, resourceId);
                }
            }
            if (resource.Links != null)
            {
                foreach (var link in resource.Links)
                {
                    link.Id = AddLink(link, libResourceType, resourceId);
                }
            }
        }

        public static int AddChapter(Chapter chapter, int bookId, int parentId)
        {
            int chapterId = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = chapter.Title;
                cmd.Parameters.Add("@Order", SqlDbType.Int).Value = chapter.Order;
                cmd.Parameters.Add("@Pages", SqlDbType.Int).Value = chapter.Pages;
                cmd.Parameters.Add("@FirstPage", SqlDbType.Int).Value = chapter.FirstPage;
                cmd.Parameters.Add("@BookID", SqlDbType.Int).Value = bookId;
                cmd.Parameters.Add("@ParentChapterID", SqlDbType.Int).Value = parentId == 0 ? DBNull.Value : (object)parentId;
                cmd.CommandText = "INSERT INTO dbo.Chapter(Title, [Order], Pages, FirstPage, BookID, ParentChapterID) " +
                                  "VALUES(@Title, @Order, @Pages, @FirstPage, @BookID, @ParentChapterID) SELECT SCOPE_IDENTITY()";
                chapterId = Convert.ToInt32(cmd.ExecuteScalar());

                if (chapter.SubChapters != null)
                {
                    foreach (var subChapter in chapter.SubChapters)
                    {
                        subChapter.Id = AddChapter(subChapter, bookId, chapterId);
                    }
                }
                connection.Close();
            }
            return chapterId;
        }

        public static int AddVideoSection(VideoSection section, int courseId, int parentId)
        {
            int sectionId = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = section.Title;
                cmd.Parameters.Add("@Order", SqlDbType.Int).Value = section.Order;
                cmd.Parameters.Add("@Length", SqlDbType.Int).Value = section.Length;
                cmd.Parameters.Add("@ParentVideoCourseID", SqlDbType.Int).Value = courseId;
                cmd.Parameters.Add("@ParentVideoSectionID", SqlDbType.Int).Value = parentId == 0 ? DBNull.Value : (object)parentId;
                cmd.CommandText = "INSERT INTO dbo.VideoSection(Title, [Order], Length, ParentVideoCourseID, ParentVideoSectionID) " +
                                  "VALUES(@Title, @Order, @Length, @ParentVideoCourseID, @ParentVideoSectionID) SELECT SCOPE_IDENTITY()";
                sectionId = Convert.ToInt32(cmd.ExecuteScalar());

                if (section.Sections != null)
                {
                    foreach (var subSection in section.Sections)
                    {
                        subSection.Id = AddVideoSection(subSection, courseId, sectionId);
                    }
                }
                if (section.Fragments != null)
                {
                    foreach (var fragment in section.Fragments)
                    {
                        fragment.Id = AddVideoFragment(fragment, sectionId);
                    }
                }
                connection.Close();
            }
            return sectionId;
        }

        public static int AddVideoFragment(VideoFragment fragment, int parentId)
        {
            int fragmentId = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = fragment.Title;
                cmd.Parameters.Add("@Order", SqlDbType.Int).Value = fragment.Order;
                cmd.Parameters.Add("@Length", SqlDbType.Int).Value = fragment.Length;
                cmd.Parameters.Add("@PathToFile", SqlDbType.NVarChar).Value = (object)fragment.PathToFile ?? DBNull.Value;
                cmd.Parameters.Add("@ParentVideoSectionID", SqlDbType.Int).Value = parentId;
                cmd.CommandText = "INSERT INTO dbo.VideoFragment(Title, [Order], Length, PathToFile, ParentVideoSectionID) " +
                                  "VALUES(@Title, @Order, @Length, @PathToFile, @ParentVideoSectionID) SELECT SCOPE_IDENTITY()";

                fragmentId = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return fragmentId;
        }

        public static void AddTheme(string theme, string resourceType, int resourceId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Theme", SqlDbType.NVarChar).Value = theme;
                cmd.CommandText = $"EXEC dbo.CreateTheme @Theme";
                cmd.ExecuteNonQuery();

                cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Theme", SqlDbType.NVarChar).Value = theme;
                cmd.CommandText = "SELECT ThemeID FROM dbo.Theme WHERE Title = @Theme";
                int themeId = Convert.ToInt32(cmd.ExecuteScalar());

                cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@ThemeID", SqlDbType.NVarChar).Value = themeId;
                cmd.Parameters.Add("@ResourceID", SqlDbType.NVarChar).Value = resourceId;
                cmd.Parameters.Add("@ResourceType", SqlDbType.NVarChar).Value = resourceType;
                cmd.CommandText = "INSERT INTO dbo.Theme_LibResource(ThemeID, ResourceID, LibResourceTypeID) " +
                                  "SELECT @ThemeID, @ResourceID, LibResourceTypeID FROM LibResourceType WHERE Title = @ResourceType";
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        public static void AddAuthor(string author, string resourceType, int resourceId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Author", SqlDbType.NVarChar).Value = author;
                cmd.CommandText = $"INSERT INTO dbo.Author(Author) VALUES(@Author) SELECT SCOPE_IDENTITY()";
                int authorId = Convert.ToInt32(cmd.ExecuteScalar());

                cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@AuthorID", SqlDbType.NVarChar).Value = authorId;
                cmd.Parameters.Add("@ResourceID", SqlDbType.NVarChar).Value = resourceId;
                cmd.Parameters.Add("@ResourceType", SqlDbType.NVarChar).Value = resourceType;
                cmd.CommandText = "INSERT INTO dbo.Author_LibResource(AuthorID, ResourceID, LibResourceTypeID) " +
                    $"SELECT @AuthorID, @ResourceID, LibResourceTypeID FROM LibResourceType WHERE Title = @ResourceType";
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        public static int AddLink(Link link, string resourceType, int resourceId)
        {
            int linkId = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = link.Title;
                cmd.Parameters.Add("@Path", SqlDbType.NVarChar).Value = link.Path;
                cmd.CommandText = $"INSERT INTO dbo.Link(Title, Path) VALUES(@Title, @Path) SELECT SCOPE_IDENTITY()";
                linkId = Convert.ToInt32(cmd.ExecuteScalar());

                cmd = new SqlCommand { Connection = connection };
                cmd.Parameters.Add("@LinkID", SqlDbType.NVarChar).Value = linkId;
                cmd.Parameters.Add("@ResourceID", SqlDbType.NVarChar).Value = resourceId;
                cmd.Parameters.Add("@ResourceType", SqlDbType.NVarChar).Value = resourceType;
                cmd.CommandText = "INSERT INTO dbo.Link_LibResource(LinkID, ResourceID, LibResourceTypeID) " +
                                  $"SELECT @LinkID, @ResourceID, LibResourceTypeID FROM LibResourceType WHERE Title = @ResourceType";
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            return linkId;
        }


        private static bool ValidateResource(LibResource resource)
        {
            if (string.IsNullOrEmpty(resource.Title)) return false;
            return true;
        }
        #endregion

        #region Read
        public static Library LoadLibrary()
        {
            //var library = new Library();
            var library = Library.GetInstance();     
            library.Resources = new List<LibResource>();
            var books = LoadBooks();
            var videos = LoadVideoCourses();
            var sites = LoadInternetResources();

            library.Resources.AddRange(books);
            library.Resources.AddRange(videos);
            library.Resources.AddRange(sites);
            return library;
        }

        private static List<string> LoadThemes(int resourceId, string libResourceType)
        {
            var themes = new List<string>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT Title FROM dbo.Theme WHERE ThemeID IN " +
                            $"(SELECT ThemeID FROM dbo.Theme_LibResource WHERE ResourceID = {resourceId} AND " +
                            $"LibResourceTypeID = (SELECT LibResourceTypeID FROM dbo.LibResourceType WHERE Title = '{libResourceType}'))";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    themes.Add(Convert.ToString(row["Title"]));
                }
                connection.Close();
            }
            return themes;
        }

        private static string[] LoadAuthors(int resourceId, string libResourceType)
        {
            var authors = new List<string>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT Author FROM dbo.Author WHERE AuthorID IN " +
                        $"(SELECT AuthorID FROM dbo.Author_LibResource WHERE ResourceID = {resourceId} AND " +
                        $"LibResourceTypeID = (SELECT LibResourceTypeID FROM dbo.LibResourceType WHERE Title = '{libResourceType}'))";
                var adapter = new SqlDataAdapter(query, connection);
                var table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    authors.Add(Convert.ToString(row["Author"]));
                }
                connection.Close();
            }
            return authors.ToArray();
        }

        private static List<Link> LoadLinks(int resourceId, string libResourceType)
        {
            var links = new List<Link>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT LinkID, Title, Path FROM dbo.Link WHERE LinkID IN " +
                            $"(SELECT LinkID FROM dbo.Link_LibResource WHERE ResourceID = {resourceId} AND " +
                            $"LibResourceTypeID = (SELECT LibResourceTypeID FROM dbo.LibResourceType WHERE Title = '{libResourceType}'))";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    Link link = new Link
                    {
                        Id = Convert.ToInt32(row["LinkID"]),
                        Title = Convert.ToString(row["Title"]),
                        Path = Convert.ToString(row["Path"])
                    };
                    links.Add(link);
                }
                connection.Close();
            }
            return links;
        }

        private static List<Book> LoadBooks()
        {
            var books = new List<Book>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT BookID, Title, Year, PathToCover, PathToDescription, Pages, FirstPage, Publishment, Edition FROM dbo.Book";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    Book book = new Book
                    {
                        Id = Convert.ToInt32(row["BookID"]),
                        Title = Convert.ToString(row["Title"]),
                        Year = Convert.ToInt32(row["Year"]),
                        PathToCover = Convert.ToString(row["PathToCover"]),
                        PathToDescription = Convert.ToString(row["PathToDescription"]),
                        Pages = Convert.ToInt32(row["Pages"]),
                        FirstPage = Convert.ToInt32(row["FirstPage"]),
                        Publishment = Convert.ToString(row["Publishment"]),
                        Edition = Convert.ToInt32(row["Edition"]),
                    };
                    books.Add(book);
                }
                foreach (var book in books)
                {
                    book.Authors = LoadAuthors(book.Id, "Book");
                    book.Links = LoadLinks(book.Id, "Book");
                    book.Themes = LoadThemes(book.Id, "Book");
                    book.Chapters = LoadChapters(book.Id, 0);
                }
                connection.Close();
            }
            return books;
        }

        private static List<Chapter> LoadChapters(int bookId, int parentId)
        {
            var chapters = new List<Chapter>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT ChapterID, Title, [Order], Pages, FirstPage, ParentChapterID, BookID FROM dbo.Chapter " +
                            $"WHERE BookID = {bookId} AND ParentChapterID ";
                var queryEnding = parentId > 0 ? $"= {parentId}" : "IS NULL";
                query += queryEnding;
                var adapter = new SqlDataAdapter(query, connection);
                var table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    Chapter chapter = new Chapter
                    {
                        Id = Convert.ToInt32(row["ChapterID"]),
                        Title = Convert.ToString(row["Title"]),
                        Order = Convert.ToInt32(row["Order"]),
                        Pages = Convert.ToInt32(row["Pages"]),
                        FirstPage = Convert.ToInt32(row["FirstPage"])
                    };
                    chapters.Add(chapter);
                }

                foreach (var chapter in chapters)
                {
                    chapter.SubChapters = LoadChapters(bookId, chapter.Id);
                }
                connection.Close();
            }
            return chapters;
        }

        private static List<VideoCourse> LoadVideoCourses()
        {
            var videos = new List<VideoCourse>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT VideoCourseID, Title, Year, PathToCover, PathToDescription, Length FROM dbo.VideoCourse";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    VideoCourse video = new VideoCourse
                    {
                        Id = Convert.ToInt32(row["VideoCourseID"]),
                        Title = Convert.ToString(row["Title"]),
                        Year = Convert.ToInt32(row["Year"]),
                        PathToCover = Convert.ToString(row["PathToCover"]),
                        PathToDescription = Convert.ToString(row["PathToDescription"]),
                        Length = Convert.ToInt32(row["Length"])
                    };
                    videos.Add(video);
                }
                foreach (var video in videos)
                {
                    video.Authors = LoadAuthors(video.Id, "VideoCourse");
                    video.Links = LoadLinks(video.Id, "VideoCourse");
                    video.Themes = LoadThemes(video.Id, "VideoCourse");
                    video.Sections = LoadSections(video.Id, 0);
                }
                connection.Close();
            }
            return videos;
        }

        private static List<VideoSection> LoadSections(int videoId, int parentId)
        {
            var sections = new List<VideoSection>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT VideoSectionID, Title, [Order], Length, ParentVideoCourseID, ParentVideoSectionID FROM dbo.VideoSection " +
                            $"WHERE ParentVideoCourseID = {videoId} AND ParentVideoSectionID ";
                var queryEnding = parentId > 0 ? $"= {parentId}" : "IS NULL";
                query += queryEnding;
                var adapter = new SqlDataAdapter(query, connection);
                var table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    VideoSection section = new VideoSection
                    {
                        Id = Convert.ToInt32(row["VideoSectionID"]),
                        Title = Convert.ToString(row["Title"]),
                        Order = Convert.ToInt32(row["Order"]),
                        Length = Convert.ToInt32(row["Length"])
                    };
                    sections.Add(section);
                }

                foreach (var section in sections)
                {
                    section.Sections = LoadSections(videoId, section.Id);
                    section.Fragments = LoadFragments(section.Id);
                }
                connection.Close();
            }
            return sections;
        }

        private static List<VideoFragment> LoadFragments(int sectionId)
        {
            var fragments = new List<VideoFragment>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT VideoFragmentID, Title, [Order], Length, PathToFile, ParentVideoSectionID FROM dbo.VideoFragment " +
                            $"WHERE ParentVideoSectionID = {sectionId}";
                var adapter = new SqlDataAdapter(query, connection);
                var table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    VideoFragment fragment = new VideoFragment
                    {
                        Id = Convert.ToInt32(row["VideoFragmentID"]),
                        Title = Convert.ToString(row["Title"]),
                        Order = Convert.ToInt32(row["Order"]),
                        Length = Convert.ToInt32(row["Length"]),
                        PathToFile = Convert.ToString(row["PathToFile"])
                    };
                    fragments.Add(fragment);
                }
                connection.Close();
            }
            return fragments;
        }

        private static List<InternetResource> LoadInternetResources()
        {
            var sites = new List<InternetResource>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT InternetResourceID, Title, Year, PathToCover, PathToDescription, MainLink FROM dbo.InternetResource";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable table = new DataTable();
                adapter.Fill(table);
                foreach (DataRow row in table.Rows)
                {
                    InternetResource site = new InternetResource
                    {
                        Id = Convert.ToInt32(row["InternetResourceID"]),
                        Title = Convert.ToString(row["Title"]),
                        Year = Convert.ToInt32(row["Year"]),
                        PathToCover = Convert.ToString(row["PathToCover"]),
                        PathToDescription = Convert.ToString(row["PathToDescription"]),
                        MainLink = Convert.ToString(row["MainLink"])
                    };
                    sites.Add(site);
                }
                foreach (var site in sites)
                {
                    site.Authors = LoadAuthors(site.Id, "InternetResource");
                    site.Links = LoadLinks(site.Id, "InternetResource");
                    site.Themes = LoadThemes(site.Id, "InternetResource");
                }
                connection.Close();
            }
            return sites;
        }
        #endregion

        #region Update
        public static bool EditResource(LibResource resource, int resourceId)
        {
            if (!ValidateResource(resource)) return false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand { Connection = connection };

                SqlParameter title = new SqlParameter("@Title", SqlDbType.NVarChar);
                SqlParameter year = new SqlParameter("@Year", SqlDbType.Int);
                SqlParameter pathToCover = new SqlParameter("@PathToCover", SqlDbType.NVarChar);
                SqlParameter pathToDescription = new SqlParameter("@PathToDescription", SqlDbType.NVarChar);

                cmd.Parameters.Add(title).Value = resource.Title;
                cmd.Parameters.Add(year).Value = resource.Year;
                cmd.Parameters.Add(pathToCover).Value = (object)resource.PathToCover ?? DBNull.Value;
                cmd.Parameters.Add(pathToDescription).Value = (object)resource.PathToDescription ?? DBNull.Value;

                string tableName, idName, varParams, libResourceType;
                switch (resource)
                {
                    case Book book:
                        libResourceType = "Book";
                        cmd.Parameters.Add("@Pages", SqlDbType.Int).Value = book.Pages;
                        cmd.Parameters.Add("@FirstPage", SqlDbType.Int).Value = book.FirstPage;
                        cmd.Parameters.Add("@Publishment", SqlDbType.NVarChar).Value = book.Publishment;
                        cmd.Parameters.Add("@Edition", SqlDbType.Int).Value = book.Edition;
                        tableName = "dbo.Book";
                        varParams = "Pages = @Pages, FirstPage = @FirstPage, Publishment = @Publishment, Edition = @Edition";
                        idName = "BookID";
                        break;
                    case VideoCourse video:
                        libResourceType = "VideoCourse";
                        cmd.Parameters.Add("@Length", SqlDbType.Int).Value = video.Length;
                        tableName = "dbo.VideoCourse";
                        varParams = "Length = @Length";
                        idName = "VideoCourseID";
                        break;
                    case InternetResource site:
                        libResourceType = "InternetResource";
                        cmd.Parameters.Add("@MainLink", SqlDbType.NVarChar).Value = site.MainLink;
                        tableName = "dbo.InternetResource";
                        varParams = "MainLink = @MainLink";
                        idName = "InternetResourceID";
                        break;
                    default: return false;
                }

                cmd.CommandText = $"UPDATE {tableName} SET Title = @Title, Year = @Year, PathToCover = @PathToCover, " +
                                  $"PathToDescription = @PathToDescription, {varParams} WHERE {idName} = {resourceId}";
                connection.Open();
                cmd.ExecuteNonQuery();

                var oldAuthors = LoadAuthors(resourceId, libResourceType);
                foreach (var old in oldAuthors)
                {
                    if (!resource.Authors.Contains(old))
                    {
                        cmd = new SqlCommand("DELETE FROM dbo.Author_LibResource WHERE AuthorID = " +
                                             $"(SELECT AuthorID FROM dbo.Author WHERE Author = '{old}') AND ResourceID = {resourceId} AND " +
                                             "LibResourceTypeID = (SELECT LibResourceTypeID FROM LibResourceType " +
                                             $"WHERE Title = '{libResourceType}')", connection);
                        cmd.ExecuteNonQuery();
                    }
                }
                foreach (var author in resource.Authors)
                {
                    if (!oldAuthors.Contains(author))
                    {
                        cmd = new SqlCommand($"INSERT INTO dbo.Author (Author) VALUES ('{author}') SELECT SCOPE_IDENTITY()", connection);
                        var authorId = Convert.ToInt32(cmd.ExecuteScalar());
                        cmd = new SqlCommand("INSERT INTO dbo.Author_LibResource (AuthorID, ResourceID, LibResourceTypeID) " +
                                             $"SELECT {authorId}, {resourceId}, LibResourceTypeID FROM LibResourceType " +
                                             $"WHERE Title = '{libResourceType}'", connection);
                        cmd.ExecuteNonQuery();
                    }
                }
                connection.Close();
            }
            return true;
        }

        #endregion

        #region Delete
        public static bool RemoveResource(LibResource resource, int resourceId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand { Connection = connection };

                string tableName, idName;
                switch (resource)
                {
                    case Book book:
                        tableName = "dbo.Book";
                        idName = "BookID";
                        break;
                    case VideoCourse video:
                        tableName = "dbo.VideoCourse";
                        idName = "VideoCourseID";
                        break;
                    case InternetResource site:
                        tableName = "dbo.InternetResource";
                        idName = "InternetResourceID";
                        break;
                    default: return false;
                }

                cmd.CommandText = $"DELETE FROM {tableName} WHERE {idName} = {resourceId}";
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            return true;
        }

        #endregion
    }
}
